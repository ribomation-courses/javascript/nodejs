const http = require('http');
const url  = require('url');
const hostname = 'localhost', port = 3000;

const server = http.createServer((req, res) => {
    const params = url.parse(req.url, true).query;
    const name   = params.name || 'Nisse';
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end(`Hi ${name.toUpperCase()}, Welcome to node! 
    The time is ${new Date().toLocaleTimeString()}`);
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
    console.log(`Invoke as http://${hostname}:${port}/?name=anna`);
});
