const { load, store } = require('./io-lib');

const srcfile = process.argv[2] || __filename;
const destfile = srcfile + '.COPY';

load(srcfile)
    .then(data => {
        console.info('loaded %s', srcfile);
        return store(destfile, data.toUpperCase());
    })
    .then(() => console.info('written %s', destfile))
    .catch(console.error)
    ;
