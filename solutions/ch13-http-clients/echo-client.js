const {request} = require('http');

const payload = process.argv[2] || 'tjabba habba babba';
const cfg = {
    host: 'localhost',
    port: 5000,
    method: 'POST',
    path: '/',
    headers: {
        'Content-Type': 'text/plain',
        'Content-Length': payload.length
    }
};
const REQ = request(cfg, (RES) => {
    console.info('RPLY: %d', RES.statusCode);
    let body = [];
    RES.on('data', chunk => body.push(chunk))
        .on('end', () => {
            body = Buffer.concat(body).toString();
            console.info('  >> %s', body);
        })
        .on('error', ERR => console.error(ERR));
});
REQ.write(payload);
REQ.end();
REQ.on('error', console.error);

