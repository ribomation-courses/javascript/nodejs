const { createServer } = require('http');
const port = 5000;

createServer((req, res) => {
    console.info('REQ: %s %s', req.method, req.url);
    let body = [];
    req.on('data', chunk => body.push(chunk))
        .on('end', () => {
            const input = Buffer.concat(body).toString();
            console.info('  >> %s', input);

            const output = input.toUpperCase();
            res.writeHead(200, {
                'Content-Type': 'text/plain',
                'Content-Length': output.length
            });
            res.end(output);
        });
})
    .listen(port, _ => {
        console.info('server started: http://localhost:%d/', port);
    });
