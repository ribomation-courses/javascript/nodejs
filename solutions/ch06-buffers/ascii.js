const lower = Buffer.alloc(26);
const upper = Buffer.alloc(26);
const digit = Buffer.alloc(10);

function print(buf) {
    for (let k=0; k<buf.length; ++k) {
        console.log('%d: %s', buf[k], String.fromCharCode(buf[k]));
    }
}

for (let k = 97, idx=0; k <= 122; ++k, ++idx) { lower[idx] = k; }
for (let k = 65, idx=0; k <= 90; ++k, ++idx)  { upper[idx] = k; }
for (let k = 48, idx=0; k <= 57; ++k, ++idx)  { digit[idx] = k; }

const N = lower.length + upper.length + digit.length;
print( Buffer.concat([digit, upper, lower], N) );
