let _factor = 10;

function multiply(n) {
    return n * _factor;
}

function factor(n) {
    _factor = n;
}

module.exports = {
    multiply: multiply,
    factor: factor
};
