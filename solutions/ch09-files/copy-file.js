const { readFile, writeFile } = require('fs');

const srcfile = process.argv[2] || __filename;
const dstfile = srcfile + '.COPY';

readFile(srcfile, (err, inbuf) => {
    if (err) {console.error(err.message); process.exit(1);}
    console.info('loaded %s', srcfile);

    const outbuf = Buffer.from(inbuf.toString().toUpperCase());
    writeFile(dstfile, outbuf, (err) => {
        if (err) {console.error(err.message); process.exit(1);}
        console.info('stored %s', dstfile);
    });
});

