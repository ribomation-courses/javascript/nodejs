const EventEmitter = require('events');

class EvenOddEmitter extends EventEmitter {
    handle(n) {
        const event = (n % 2 === 0) ? 'even' : 'odd';
        this.emit(event, n);
    }
}

const randInt = (lb = 1, ub = 1000) => {
    const width = ub - lb + 1;
    const r     = Math.random() * width;
    return lb + Math.floor(r);
};

const delay    = 250;
const duration = 5 * 1000;
const emmi     = new EvenOddEmitter();
const evenLst  = [];
const oddLst   = [];

emmi.on('even', (n) => { evenLst.push(n); });
emmi.on('odd' , (n) => { oddLst.push(n); });

const intvId = setInterval(() => {
    const num = randInt();
    console.log('num: %d', num);
    emmi.handle(num);
}, delay);

setTimeout(() => {
    clearInterval(intvId);
    emmi.removeAllListeners();
    console.log('even: %s', evenLst);
    console.log('odd : %s', oddLst);
}, duration);

