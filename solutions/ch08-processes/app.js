const { fork } = require('child_process');

const delay    = 1000;
const duration = 10 * delay;
const worker   = fork('./worker');

const itvId = setInterval(() => {
    const arg = genTxt();
    worker.send(arg);
}, delay);

setTimeout(() => {
    clearInterval(itvId);
    process.kill(worker.pid, 'SIGHUP');
}, duration);

worker.on('message', (reply) => {
    console.log('[parent] %s', reply);
});

worker.on('exit', (code) => {
    console.log('[parent] child done: code=%d', code);
});

const words = [
    'tjabba', 'habba', 'babba', 
    'foobar', 'hello', 'howdy'
];
let wordIdx = 0;

function genTxt() {
    return words[wordIdx++ % words.length]
}
