const { createServer } = require('http');
const transform = require('through2');
const port = 6000;

createServer((req, res) => {
    console.debug('REQ: %s %s', req.method, req.url);
    req
        .pipe(
            transform(function (data, enc, next) {
                this.push(data.toString().toUpperCase());
                next();
            })
        )
        .pipe(res);
})
    .listen(port, _ => {
        console.info('server started: http://localhost:%d/', port);
    });
