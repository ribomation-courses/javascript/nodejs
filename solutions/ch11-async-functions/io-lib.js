const { readFile, writeFile } = require('fs');

function load(path) {
    return new Promise((succ, fail) => {
        readFile(path, 'utf8', (err, data) => {
            if (err) fail(err.message);
            else succ(data);
        });
    });
}

function store(path, content) {
    return new Promise((succ, fail) => {
        writeFile(path, content, (err) => {
            if (err) fail(err.message);
            else succ();
        });
    });
}

module.exports = { load, store };
