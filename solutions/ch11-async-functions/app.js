const { load, store } = require('./io-lib');

async function copy(src, dst) {
    try {
        const content = await load(src);
        console.info('loaded %s', src);

        await store(dst, content.toUpperCase());
        console.info('written %s', dst);
    } catch (err) {
        console.error(err);
    }
}

const srcfile = process.argv[2] || __filename;
const dstfile = srcfile + '.COPY';

copy(srcfile, dstfile);
