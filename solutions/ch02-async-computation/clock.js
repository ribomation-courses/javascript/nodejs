const clock = (id) => {
    const tab = "    ".repeat(id-1);
    return () => {
        const time = new Date().toISOString().substr(11, 12);
        console.info('%s [%d] %s', tab, id, time);
    }
};

setInterval(clock(1), 500);
setInterval(clock(2), 1000);
setInterval(clock(3), 2000);

setTimeout(() => {
    process.exit(0);
}, 10000);
