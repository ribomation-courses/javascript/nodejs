const fs = require('fs');

function tinyRequire(path) {
    const code = fs.readFileSync(path, 'utf8');
    const iffe = `(function(tinyModule, tinyScript){ 
        ${code}
        return tinyModule;
    }({}, '${path}'))`;
    return eval(iffe).exports;
}

const {answer, sum, file} = tinyRequire('./tiny-module.js');

console.log('answer  = %d', answer);
console.log('file    = %s', file);
console.log('sum(10) = %d', sum(10));
