
export function sum(n) { return n * (n + 1) / 2; }

export class Account {
    constructor(_balance, _rate) {
        this.balance = _balance;
        this.rate = _rate;
    }
}

