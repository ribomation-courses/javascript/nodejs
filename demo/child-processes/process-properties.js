Object.keys(process).forEach(key => {
    let val  = process[key];
    let type = typeof val;
    if (type === 'function') val = 'fn';
    else {
        try {
            val = JSON.stringify(val).substr(0, 50);
        } catch (e) {
            val = e.message;
        }
    }
    console.log(`process[${key}]: ${type} = ${val}`);
});
