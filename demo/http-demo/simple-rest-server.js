const {createServer} = require('http');
const {parse}        = require('url');

const port     = 8000;
const jsonType = 'application/json';
const db       = [
    {id: 1, name: 'Anna Conda', age: 42},
    {id: 2, name: 'Justin Time', age: 37}
];

createServer((req, res) => {
    const op   = req.method;
    const file = parse(req.url, true).pathname;
    console.debug('REQ: %s %s', op, file);
    if (file.startsWith('/persons')) {
        personsResource(op, file, req, res);
    } else {
        res.writeHead(400).end();
    }
}).listen(port, _ => {
    console.info(`started: http://localhost:${port}/persons[/id]`);
});


function personsResource(op, file, req, res) {
    const m  = file.match(/.(\d+)$/);
    const id = m ? m[1] : undefined;
    switch (op) { //CRUD
        case 'POST':
            personsCREATE(req, res);
            break;

        case 'GET':
            personsREAD(id, res);
            break;

        case 'PUT':
            personsUPDATE(id, req, res);
            break;

        case 'DELETE':
            personsDELETE(id, res);
            break;
    }
}

function personsREAD(id, res) {
    if (id) {
        id      = Number(id);
        payload = db.find(o => o.id === id);
    } else {
        payload = db;
    }
    if (!payload) {
        res.writeHead(404).end();
    } else {
        payload = JSON.stringify(payload);
        res.writeHead(200, {
            'Content-Type': jsonType,
            'Content-Length': payload.length
        }).end(payload);
    }
}

function personsCREATE(req, res) {
    let body = [];
    req.on('data', chunk => body.push(chunk))
        .on('end', () => {
            const obj = JSON.parse(Buffer.concat(body).toString());
            console.debug('  >> %o', obj);

            obj.id = 1 + Math.max(...db.map(o => o.id));
            db.push(obj);

            const payload = JSON.stringify(obj);
            res.writeHead(201, {
                'Content-Type': jsonType,
                'Content-Length': payload.length
            }).end(payload);
        });
}

function personsDELETE(id, res) {
    id        = Number(id);
    const idx = db.findIndex(o => o.id === id);
    if (idx === -1) {
        res.writeHead(404).end();
    } else {
        db.splice(idx, 1);
        res.writeHead(204).end();
    }
}

function personsUPDATE(id, req, res) {
    id        = Number(id);
    const idx = db.findIndex(o => o.id === id);
    if (idx === -1) {
        res.writeHead(404).end();
    } else {
        let body = [];
        req.on('data', chunk => body.push(chunk))
            .on('end', () => {
                const objUpdate = JSON.parse(Buffer.concat(body).toString());
                console.debug('  >> %o', objUpdate);

                const objOrig = db[idx];
                ['name', 'age'].forEach(key => {
                    if (objUpdate[key]) objOrig[key] = objUpdate[key];
                });

                const payload = JSON.stringify(objOrig);
                res.writeHead(200, {
                    'Content-Type': jsonType,
                    'Content-Length': payload.length
                }).end(payload);
            });
    }
}


