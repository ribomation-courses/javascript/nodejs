const {get} = require('https');

const url = 'https://www.ribomation.se/courses/javascript/nodejs.html';

const invalidHost = 'https://nope.ribomation.se/';
get(invalidHost, res => {
    console.log('statusCode:', res.statusCode);
    console.log('headers:', res.headers);
    let chunk = 1;
    res.on('data', buf => {
        const payload = buf
            .toString()
            .substr(0, 80)
            .replace(/\n+/g, '')
            .replace(/\s+/g, ' ')
        ;
        process.stdout.write(`${chunk++}: ${payload}...\n`);
    });
    res.on('end', () => {
        process.stdout.write('-- done ---');
    });
})
.on('error', err => {
    console.error('***', err);
});

