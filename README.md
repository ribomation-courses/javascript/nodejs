# Node.js, 2 days

Welcome to this course.
The syllabus can be found at
[javascript/nodejs](https://www.ribomation.se/courses/javascript/nodejs.html)

Here you can find
* Installation instructions
* Solutions to the exercises
* Demo programs

# Installation Instructions

In order to do the programming exercises of the course, 
you need to have the following programs/tools installed:

## Node.js

* [Node JS](https://nodejs.org/en/download/)

The installer, will install the following executables.
Verify that they are available

    node --version
    npm --version
    npx --version

## IDE

You need an editor or IDE to write you programs. If your
already have an favorite; then use it. Otherwise, you can
install one of or both of

* [MS Visual Code](https://code.visualstudio.com/Download) 
* [JetBrains WebStorm (_30 days trial_)](https://www.jetbrains.com/webstorm/download)

## Browser

A few exercises involve a browser. Go for a modern one, such as

* [Google Chrome](https://www.google.se/chrome/browser/desktop/) 
* [Mozilla Firefox](https://www.mozilla.org/sv-SE/firefox/new/)
* [Microsoft Edge](https://www.microsoft.com/sv-se/windows/microsoft-edge)

# HTTP Client

A few exercises involve a HTTP client. If you already have a 
favorite use it. Otherwise install one of

* [HTTPie](https://httpie.org/)
    * Based on [Python](https://www.python.org/)
* [cURL](https://curl.haxx.se/)
* [POSTman](https://www.getpostman.com/)


## GIT

You need to have a GIT client installed to clone this repo.
Otherwise, you can just click on the download button and grab
it all as a ZIP or TAR bundle.

* [GIT Client](https://git-scm.com/downloads)

## Usage of this GIT Repo

Create first a base folder for this course, somewhere.

    mkdir -p ~/nodejs-course/my-solutions

Then clone this repo, into the base folder

    cd ~/nodejs-course
    git clone https://gitlab.com/ribomation-courses/javascript/nodejs.git gitlab

Solutions will be push:ed to this repo during the course.
Get the latest updates by a `git pull` operation

    cd ~/nodejs-course/gitlab
    git pull

# Running solutions and demos

If the program folder has a file named `package.json` and
it contains dependencies run the following command to download
the dependent packages, inside the chosen program folder.

    npm install

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

